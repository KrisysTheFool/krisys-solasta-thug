﻿using KrisysSolastaRoguishThug.Utility;
using SolastaModApi;
using SolastaModApi.Extensions;
using System;

namespace solasta_roguish_thug.Subclass
{
    class RoguishThugSubclass
    {
        public static int THUG_BONUS_SHOVE_ACTION_ID = 800;
        //public static int THUG_CUNNING_SHOVE_ACTION_ID = 801;

        private static readonly Guid KrisysRogueSubclassThugGuid = new Guid("19ccafeb-7348-4e24-b9a9-7f990918a49f");
        private static readonly string KrisysRogueSubclassThugName = "KrisysRogueSubclassThug";
        private static readonly string KrisysRogueSubclassThugNameGuid = GuidHelper.Create(KrisysRogueSubclassThugGuid, KrisysRogueSubclassThugName).ToString();

        public static void BuildAndAddToClass()
        {
            Build();
            DatabaseHelper.FeatureDefinitionSubclassChoices.SubclassChoiceRogueRoguishArchetypes.Subclasses.Add(KrisysRogueSubclassThugName);
        }

        private static CharacterSubclassDefinition Build()
        {
            GuiPresentation subclassGuiPresentation = new GuiPresentationBuilder(
                    "Subclass/&KrisysRogueSubclassThugDescription",
                    "Subclass/&KrisysRogueSubclassThugTitle")
                    .SetSpriteReference(DatabaseHelper.CharacterSubclassDefinitions.MartialChampion.GuiPresentation.SpriteReference)
                    .Build();

            NegativeFeatureUtility.FeatureToNegativeGuidDictionary.Add(DatabaseHelper.FeatureDefinitionAdditionalDamages.AdditionalDamageRogueSneakAttack.GUID, "2f1bf6b3-a727-4da5-b24e-ec7e9b8decaf");

            CharacterSubclassDefinition definition = new CharacterSubclassDefinitionBuilder(KrisysRogueSubclassThugName, KrisysRogueSubclassThugNameGuid)
                    .SetGuiPresentation(subclassGuiPresentation)
                    .AddFeatureAtLevel(NegativeFeatureBuilder.CreateAndAddToDB(DatabaseHelper.FeatureDefinitionAdditionalDamages.AdditionalDamageRogueSneakAttack), 3)
                    .AddFeatureAtLevel(KrisysRogueSubclassThugExploitVulnerabilitiesSneakAttackBuilder.KrisysExploitVulnerabilities, 3)
                    .AddFeatureAtLevel(KrisysRogueSubclassThugProficienciesBuilder.KrisysThugProficiencies, 3)
                    .AddFeatureAtLevel(KrisysRogueSubclassThugBrutalMethodsBuilder.KrisysThugBrutalMethods, 9)
                    .AddFeatureAtLevel(KrisysRogueSubclassThugOvercomeCompetitionBuilder.KrisysThugOvercomeCompetition, 13)
                    .AddToDB();

            return definition;
        }

        private class KrisysRogueSubclassThugExploitVulnerabilitiesSneakAttackBuilder : BaseDefinitionBuilder<FeatureDefinitionAdditionalDamage>
        {
            private static readonly string KrisysExploitVulnerabilitiesGuid = "26373cfd-96c0-49c0-a31b-0c02bccaa312";
            private static readonly string KrisysExploitVulnerabilitiesName = "KrisysRogueSubclassThugExploitVulnerabilities";

            protected KrisysRogueSubclassThugExploitVulnerabilitiesSneakAttackBuilder(string name, string guid) : base(DatabaseHelper.FeatureDefinitionAdditionalDamages.AdditionalDamageRogueSneakAttack, name, guid)
            {
                Definition.GuiPresentation.Title = "Feature/&KrisysRogueSubclassThugExploitVulnerabilitiesSneakAttackTitle";
                Definition.GuiPresentation.Description = "Feature/&KrisysRogueSubclassThugExploitVulnerabilitiesSneakAttackDescription";

                Definition.SetRequiredProperty(RuleDefinitions.AdditionalDamageRequiredProperty.None);
            }

            public static FeatureDefinitionAdditionalDamage CreateAndAddToDB(string name, string guid)
                => new KrisysRogueSubclassThugExploitVulnerabilitiesSneakAttackBuilder(name, guid).AddToDB();

            public static FeatureDefinitionAdditionalDamage KrisysExploitVulnerabilities
                = CreateAndAddToDB(KrisysExploitVulnerabilitiesName, KrisysExploitVulnerabilitiesGuid);
        }

        private class KrisysRogueSubclassThugProficienciesBuilder : BaseDefinitionBuilder<FeatureDefinitionProficiency>
        {
            private static readonly string KrisysRogueSubclassThugProficienciesGuid = "af0e91c3-9efc-40f3-be28-82380a80d676";
            private static readonly string KrisysRogueSubclassThugProficienciesName = "KrisysRogueSubclassThugProficiencies";

            protected KrisysRogueSubclassThugProficienciesBuilder(string name, string guid) : base (name, guid)
            {
                Definition.GuiPresentation.Title = "Feature/&KrisysRogueSubclassThugProficienciesTitle";
                Definition.GuiPresentation.Description = "Feature/&KrisysRogueSubclassThugProficienciesDescription";

                Definition.SetProficiencyType(RuleDefinitions.ProficiencyType.Armor);
                Definition.Proficiencies.Add("MediumArmorCategory");
                Definition.Proficiencies.Add("ShieldCategory");
            }

            public static FeatureDefinitionProficiency CreateAndAddToDB(string name, string guid)
                => new KrisysRogueSubclassThugProficienciesBuilder(name, guid).AddToDB();

            public static FeatureDefinitionProficiency KrisysThugProficiencies
                = CreateAndAddToDB(KrisysRogueSubclassThugProficienciesName, KrisysRogueSubclassThugProficienciesGuid);
        }

        private class KrisysRogueSubclassThugBrutalMethodsBuilder : BaseDefinitionBuilder<FeatureDefinitionActionAffinity>
        {
            private static readonly string KrisysRogueSubclassThugBrutalMethodsGuid = "c9568d1d-c6c2-49cb-893d-e184866b0680";
            private static readonly string KrisysRogueSubclassThugBrutalMethodsName = "KrisysRogueSubclassThugBrutalMethods";

            protected KrisysRogueSubclassThugBrutalMethodsBuilder(string name, string guid) : base (DatabaseHelper.FeatureDefinitionActionAffinitys.ActionAffinityThiefFastHands, name, guid)
            {
                Definition.GuiPresentation.Title = "Feature/&KrisysRogueSubclassThugBrutalMethodsTitle";
                Definition.GuiPresentation.Description = "Feature/&KrisysRogueSubclassThugBrutalMethodsDescription";

                Definition.AuthorizedActions.Clear();
                Definition.AuthorizedActions.Add(KrisysRogueSubclassThugBrutalMethodsActionBuilder.KrisysThugBrutalMethodsAction.Id);
            }

            public static FeatureDefinitionActionAffinity CreateAndAddToDB(string name, string guid)
                => new KrisysRogueSubclassThugBrutalMethodsBuilder(name, guid).AddToDB();

            public static FeatureDefinitionActionAffinity KrisysThugBrutalMethods
                = CreateAndAddToDB(KrisysRogueSubclassThugBrutalMethodsName, KrisysRogueSubclassThugBrutalMethodsGuid);
        }

        //private class KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionBuilder : BaseDefinitionBuilder<FeatureDefinitionAdditionalAction>
        //{
        //    private static readonly string KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionGuid = "cf366103-21de-468e-9f26-261ec551534d";
        //    private static readonly string KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionName = "KrisysRogueSubclassThugBrutalMethodsCunningAdditionalAction";

        //    protected KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionBuilder(string name, string guid) : base(DatabaseHelper.FeatureDefinitionAdditionalActions.AdditionalActionCunningFastHands)
        //    {
        //        Definition.AuthorizedActions.Clear();
        //        Definition.AuthorizedActions.Add(KrisysRogueSubclassThugBrutalMethodsActionBuilder.KrisysThugBrutalMethodsAction.Id);
        //    }

        //    public static FeatureDefinitionAdditionalAction CreateAndAddToDB(string name, string guid)
        //        => new KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionBuilder(name, guid).AddToDB();

        //    public static FeatureDefinitionAdditionalAction KrisysThugBrutalMethodsCunningAdditionalAction
        //        = CreateAndAddToDB(KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionName, KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionGuid);
        //}

        //private class KrisysRogueSubclassThugBrutalMethodsCunningConditionBuilder : BaseDefinitionBuilder<ConditionDefinition>
        //{
        //    private static readonly string KrisysRogueSubclassThugOvercomeCompetitionGuid = "89c5234e-d195-43ee-a5c1-7b42b45a044c";
        //    private static readonly string KrisysRogueSubclassThugOvercomeCompetitionName = "KrisysRogueSubclassThugBrutalMethodsCunningCondition";

        //    protected KrisysRogueSubclassThugBrutalMethodsCunningConditionBuilder(string name, string guid) : base(DatabaseHelper.ConditionDefinitions.ConditionActingCunninglyFastHands, name, guid)
        //    {
        //        Definition.Features.Clear();
        //        Definition.Features.Add(KrisysRogueSubclassThugBrutalMethodsCunningAdditionalActionBuilder.KrisysThugBrutalMethodsCunningAdditionalAction);
        //    }

        //    public static ConditionDefinition CreateAndAddToDB(string name, string guid)
        //        => new KrisysRogueSubclassThugBrutalMethodsCunningConditionBuilder(name, guid).AddToDB();

        //    public static ConditionDefinition KrisysThugBrutalMethodsCunningCondition
        //        = CreateAndAddToDB(KrisysRogueSubclassThugOvercomeCompetitionName, KrisysRogueSubclassThugOvercomeCompetitionGuid);
        //}

        //private class KrisysRogueSubclassThugBrutalMethodsCunningActionBuilder : BaseDefinitionBuilder<ActionDefinition>
        //{
        //    private static readonly string KrisysRogueSubclassThugBrutalMethodsCunningActionGuid = "d8e4953f-0061-4d4c-9abe-e04ecf8e8541";
        //    private static readonly string KrisysRogueSubclassThugBrutalMethodsCunningActionName = "KrisysRogueSubclassThugBrutalMethodsAction";

        //    protected KrisysRogueSubclassThugBrutalMethodsCunningActionBuilder(string name, string guid) : base(DatabaseHelper.ActionDefinitions.CunningActionFastHands, name, guid)
        //    {
        //        Definition.GuiPresentation.Title = "Feature/&KrisysRogueSubclassThugBrutalMethodsCunningActionTitle";
        //        Definition.GuiPresentation.Description = "Feature/&KrisysRogueSubclassThugBrutalMethodsCunningActionDescription";

        //        Definition.SetId((ActionDefinitions.Id)THUG_CUNNING_SHOVE_ACTION_ID);
        //        Definition.SetAddedConditionName(KrisysRogueSubclassThugBrutalMethodsCunningConditionBuilder.KrisysThugBrutalMethodsCunningCondition.Name);
        //    }

        //    public static ActionDefinition CreateAndAddToDB(string name, string guid)
        //        => new KrisysRogueSubclassThugBrutalMethodsCunningActionBuilder(name, guid).AddToDB();

        //    public static ActionDefinition KrisysThugBrutalMethodsCunningAction
        //        = CreateAndAddToDB(KrisysRogueSubclassThugBrutalMethodsCunningActionName, KrisysRogueSubclassThugBrutalMethodsCunningActionGuid);
        //}

        private class KrisysRogueSubclassThugBrutalMethodsActionBuilder : BaseDefinitionBuilder<ActionDefinition>
        {
            private static readonly string KrisysRogueSubclassThugBrutalMethodsActionGuid = "d8e4953f-0061-4d4c-9abe-e04ecf8e8541";
            private static readonly string KrisysRogueSubclassThugBrutalMethodsActionName = "KrisysRogueSubclassThugBrutalMethodsAction";

            protected KrisysRogueSubclassThugBrutalMethodsActionBuilder(string name, string guid) : base(DatabaseHelper.ActionDefinitions.ShoveBonus, name, guid)
            {
                Definition.SetId((ActionDefinitions.Id)THUG_BONUS_SHOVE_ACTION_ID);
            }

            public static ActionDefinition CreateAndAddToDB(string name, string guid)
                => new KrisysRogueSubclassThugBrutalMethodsActionBuilder(name, guid).AddToDB();

            public static ActionDefinition KrisysThugBrutalMethodsAction
                = CreateAndAddToDB(KrisysRogueSubclassThugBrutalMethodsActionName, KrisysRogueSubclassThugBrutalMethodsActionGuid);
        }

        private class KrisysRogueSubclassThugOvercomeCompetitionBuilder : BaseDefinitionBuilder<FeatureDefinitionAbilityCheckAffinity>
        {
            private static readonly string KrisysRogueSubclassThugOvercomeCompetitionGuid = "07b8292c-015f-4917-b7f0-a58c5218c52f";
            private static readonly string KrisysRogueSubclassThugOvercomeCompetitionName = "KrisysRogueSubclassThugOvercomeCompetition";

            protected KrisysRogueSubclassThugOvercomeCompetitionBuilder(string name, string guid) : base(name, guid)
            {
                Definition.GuiPresentation.Title = "Feature/&KrisysRogueSubclassThugOvercomeCompetitionTitle";
                Definition.GuiPresentation.Description = "Feature/&KrisysRogueSubclassThugOvercomeCompetitionDescription";

                Definition.AffinityGroups.Add(new FeatureDefinitionAbilityCheckAffinity.AbilityCheckAffinityGroup()
                {
                    affinity = RuleDefinitions.CharacterAbilityCheckAffinity.Advantage,
                    proficiencyName = DatabaseHelper.SkillDefinitions.Athletics.Name,
                    abilityScoreName = DatabaseHelper.SkillDefinitions.Athletics.AbilityScore
                });
            }

            public static FeatureDefinitionAbilityCheckAffinity CreateAndAddToDB(string name, string guid)
                => new KrisysRogueSubclassThugOvercomeCompetitionBuilder(name, guid).AddToDB();

            public static FeatureDefinitionAbilityCheckAffinity KrisysThugOvercomeCompetition
                => CreateAndAddToDB(KrisysRogueSubclassThugOvercomeCompetitionName, KrisysRogueSubclassThugOvercomeCompetitionGuid);
        }
    }
}
