﻿using SolastaModApi;
using SolastaModApi.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrisysSolastaRoguishThug.Utility
{
    public class NegativeFeatureDefinition : FeatureDefinition
    {
        public FeatureDefinition FeatureToRemove;
    }

    public class NegativeFeatureUtility
    {
        private static Dictionary<string, string> featureToNegativeGuidDictionary;

        public static Dictionary<string, string> FeatureToNegativeGuidDictionary
        {
            get
            {
                if (featureToNegativeGuidDictionary == null)
                {
                    featureToNegativeGuidDictionary = new Dictionary<string, string>();
                }
                return featureToNegativeGuidDictionary;
            }
        }
    }

    public class NegativeFeatureBuilder : BaseDefinitionBuilder<NegativeFeatureDefinition>
    {
        public NegativeFeatureBuilder(FeatureDefinition featureToRemove) : 
            base(
                $"{featureToRemove.Name}Negative",
                NegativeFeatureUtility.FeatureToNegativeGuidDictionary.ContainsKey(featureToRemove.GUID) 
                ? NegativeFeatureUtility.FeatureToNegativeGuidDictionary[featureToRemove.GUID] 
                : "")
        {
            Definition.FeatureToRemove = featureToRemove;
            Definition.GuiPresentation.SetHidden(true);
        }

        public static NegativeFeatureDefinition CreateAndAddToDB(FeatureDefinition featureToRemove)
            => new NegativeFeatureBuilder(featureToRemove).AddToDB();
    }
}
