# Solasta Roguish Thug

Mod for Solasta: Crown of the Magister.
New subclass for rogue characters.
The subclass features are only covered up to level 16. The level 17 isn't implemented yet.

Made with the Solasta Mod Template https://github.com/SolastaMods/SolastaModTemplate